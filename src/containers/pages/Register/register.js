import React from 'react'
import { Button, Typography, Box, Grid } from '@mui/material'
import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CircularProgress from '@mui/material/CircularProgress';

import { useState } from 'react';
import firebase from '../../../config/firebase/index';



export default function Register() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  const handleChangeText = (event) => {
    setEmail(event.target.value);
  };

  const handleChangeText1 = (event) => {
    setPassword(event.target.value);
  };

  // const handleRegisterSubmit = (event) => {
  //   event.preventDefault();
  //   // console.log(`Email: ${email}`);
  //   // console.log(`Password: ${password}`);
  //   console.log('data before send: ', email, password)

  //   firebase.auth().createUserWithEmailAndPassword(email, password)
  //     .then((userCredential) => {
  //       // Signed in 
  //       var user = userCredential.user;
  //       console.log('success: ', user);
  //       // ...
  //     })
  //     .catch((error) => {
  //       var errorCode = error.code;
  //       var errorMessage = error.message;
  //       console.log(errorCode, errorMessage);
  //     });
  // };

// buat tombol register berubah menjadi loading
  const [isLoading, setIsLoading] = useState(false);

  const handleRegisterClick = () => {
    setIsLoading(true);
    console.log(`Email: ${email}`);
    console.log(`Password: ${password}`);

    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in 
        var user = userCredential.user;
        console.log('success: ', user);
        // ...
      })

      .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorCode, errorMessage);
    })
    .finally(() => {
      setIsLoading(false);
    });

  };


  return (
    <div>
        <Grid container direction='row' justifyContent='center' alignItems='center' sx={{minHeight: '100vh'}}>
          <Grid item>
            <Card sx={{ maxWidth: 345 }}>
            <CardContent>
              <Typography color='#1565c0' variant="h6">Register Page</Typography>
              <Box pt={1}>
                <TextField autoComplete="off" sx={{width: 320}} id="email"  placeholder="Email" variant="outlined" onChange={handleChangeText} />
                <TextField 
                  sx={{paddingTop: 1, width: 320}} 
                  id="password"  
                  placeholder="Password" 
                  variant="outlined" 
                  type="password" 
                  autoComplete="current-password" 
                  onChange={handleChangeText1}
                  // autoComplete="off"
                />
              </Box>
            </CardContent>
            <CardActions disableSpacing sx={{justifyContent: 'center'}}>
              <Button variant="contained" onClick={handleRegisterClick}
              > 
                {isLoading && <CircularProgress size={24} color="inherit" />}
                {!isLoading ? 'Register' : 'Loading...'}
              </Button>
            </CardActions>
            </Card>
          </Grid>
          {/* <Button>Go to Dashboard</Button> */}
        </Grid>
    </div>
  )
}
