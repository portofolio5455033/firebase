import React from 'react';
import { Button, Typography, Grid, TextField, Divider, Container, Box } from '@mui/material';


import firebase, { googleProvider } from '../../../config/firebase/index';

import { useState, useEffect } from 'react';
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

import { useNavigate } from "react-router-dom";

// table
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


// const handlechangesimpan = (data) => {
//   firebase.database().ref('notes/' + data.userId).set({
//     title: data.title,
//     content: data.content,
//     date: data.date
//   });
// };

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Dashboard() {

  const [users, setUsers] = useState([]);
  const [userName, setUserName] = useState("");
  const [showWelcome, setShowWelcome] = useState(false);

  // const [simpan, setSimpan] = useState(false);
  // const [newUser, setNewUser] = useState({ title: '', content: '' })


  useEffect(() => {
    const getUsers = async () => {
      const usersRef = firebase.firestore().collection('users');
      const snapshot = await usersRef.get();
      const usersList = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setUsers(usersList);
    };

    getUsers();
  }, []);

  // menyimpan user dilocal storage
  useEffect(() => {
    const displayName = localStorage.getItem('userDisplayName');
    if (displayName) {
      setUserName(displayName);
      setShowWelcome(true);
    }
  }, []);

  const handleCloseSnackbar = () => {
    setShowWelcome(false);
  }


  const navigate = useNavigate();
  // kalau bukan user maka dia akan terpental ke halaman login lagi
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        navigate('/')
      }
    })
  }, []);

  const handleLogout = () => {
    firebase.auth().signOut();
  };

  // data damie
  // const [data, setData] = useState([]);
  // useEffect(() => {
  //   const fetchData = async () => {
  //     try {
  //       const response = await fetch('http://localhost:3031/users');
  //       const jsonData = await response.json();
  //       setData(jsonData);
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   };
  //   fetchData();
  // }, [])

  // membuat tombol simpan menjadi menampilkan data
  // const hendleSimpan = async (e) => {
  //   e.preventDefault();
  //   try {
  //     const response = await fetch('http://localhost:3031/users', {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(newUser),
  //     });
  //     const jsonData = await response.json();
  //     setSimpan(true);
  //     setData([...data, jsonData]);
  //     setNewUser({ title: '', content: '' });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  // const handleInputChange = (event) => {
  //   const {title, value} = event.target;
  //   setNewUser({ ...newUser, [title]: value });
  // };

  const [data, setData] = useState([]);
  const [newTitle, setNewTitle] = useState("");
  const [newContent, setNewContent] = useState("");
  const [addedData, setAddedData] = useState(null);

  useEffect(() => {
    fetch('http://localhost:3031/users')
      .then((response) => response.json())
      .then((data) => setData(data));
  }, [addedData]);


  // menghubungkan ke JSON server
  const handleSubmit = (e) => {
    e.preventDefault();

    const newPost = { title: newTitle, content: newContent };
    //  new Date()

    fetch("http://localhost:3031/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newPost),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setNewTitle("");
        setNewContent("");
        setAddedData(data); // memperbarui state addedData dengan data baru
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };


  return (
    <Box>
      <Button onClick={handleLogout}>Log Out</Button>
      <Grid container direction="column" justifyContent="center" alignItems="center" spacing={2} pt={2}>

        <Grid item>
          <TextField
            sx={{ width: 500 }}
            id="title"
            placeholder="title"
            variant="outlined"
            autoComplete="off"
            // value={newUser.title}
            // onChange={handleInputChange}
            value={newTitle}
            onChange={(e) => setNewTitle(e.target.value)}
          />
        </Grid>

        <Grid item>
          <TextField
            sx={{ width: 500 }}
            id="outlined-multiline-static"
            placeholder="content"
            multiline
            rows={10}
            // value={newUser.content}
            // onChange={handleInputChange}
            value={newContent}
            onChange={(e) => setNewContent(e.target.value)}
          />
        </Grid>

        <Grid item>
          <Button variant="outlined" color="success" onClick={handleSubmit}>SIMPAN</Button>
        </Grid>

        {/* {simpan && (
          <Typography variant="h6" style={{ color: 'green' }}>
            Data successfully saved!
          </Typography>
        )} */}

        <Container maxWidth='lg'>
          <Divider orientation="horizontal" sx={{ width: '100%', borderColor: 'black', pt: 2 }} />
          {/* <Card sx={{ maxWidth: '100%' }}>
            <CardContent>
              {data && 
                data.map((item) => (
                  <div key={item.id}>
                    <Typography component="div" variant="h5">{item.title}</Typography> */}

                    {/* <Typography sx={{ mt: -1 }} variant="subtitle1" color="text.secondary" component="div">
                    1 Jan 2023
                  </Typography> */}
                  
                    {/* <Typography>{item.content}</Typography>
                  </div>
                ))}
            </CardContent>
            <CardContent>
              <Typography>Content Notes</Typography>
            </CardContent>
          </Card> */}

          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align='left'>
                    <Typography variant="subtitle1" fontWeight='bold'>Title</Typography>
                  </TableCell>
                  <TableCell align='center'>
                    <Typography variant="subtitle1" fontWeight='bold'>Content</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle1" fontWeight='bold'>Detail</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data &&
                  data.map((item) => (
                  <TableRow
                    key={item.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" align='left'>{item.title}</TableCell>
                    <TableCell align="center">{item.content}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
      </Grid>

      
      <Snackbar open={showWelcome} autoHideDuration={5000} onClose={handleCloseSnackbar}>
        <MuiAlert onClose={handleCloseSnackbar} severity="success">
          Welcome, {userName}!
        </MuiAlert>
      </Snackbar>
    </Box>
  )
}
